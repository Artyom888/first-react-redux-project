import * as types from '../actions/types';
import axios from 'axios';

const getAllFriends = (name) => {
    return (dispatch) => {
        axios.get(types.FRIENDS_API_URL)
            .then(function (response) {
                dispatch({type:types.GET_ALL_FRIENDS, payload:response.data.reverse()});
            })
            .catch(function (error) {
                console.log(error);
            });
    };
};


const addFriend = (name) => {
    return (dispatch) => {
        const data = {
            name:name,
            favorite:false
        };

    axios.post(types.FRIENDS_API_URL , data )
        .then(function (response) {
            console.log(response.data);
            dispatch({type:types.ADD_FRIEND, payload:response.data});
        })
        .catch(function (error) {
            console.log(error);
        });
    };
};

const deleteFriend =(id) => {
    return (dispatch) => {

        axios.delete(types.FRIENDS_API_URL + '/' + id )
            .then(function (response) {
                dispatch({
                    type: types.DELETE_FRIEND,
                    id
                });            })
            .catch(function (error) {
                console.log(error);
            });
    };
};

const favoriteFriend = (id,favorite) => {
    return (dispatch) => {
        axios.put(types.FRIENDS_API_URL + '/' + id ,{
            favorite:favorite
        })
        .then(function (response) {
            dispatch({
                type: types.FAVORITE_FRIEND,
                field:response.data
            });
             })
        .catch(function (error) {
            console.log(error);
        });
    };
};


export { getAllFriends,addFriend,deleteFriend,favoriteFriend };
