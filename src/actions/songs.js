import * as types from './types';
import axios from 'axios';


const addSongs = (file) => {
    return (dispatch) => {
        const formData = new FormData();
        formData.append('music',file);

        // dispatch({type:types.ADD_SONG_PENDING});
       const config = {
            onUploadProgress: (progressEvent) => {
                let percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
                console.log('%=============',percentCompleted );
                dispatch({type: types.ADD_SONG_PENDING, payload: percentCompleted});
            }
        };
        axios.post(types.MUSICS_API_URL, formData,config)
            .then(function (response) {
            const payload = {
                name:response.data['fileName']
            };
            dispatch({type:types.ADD_SONG_SUCCESS, payload});
        })
            .catch(function (error) {
                dispatch({type:types.ADD_SONG_REJECTED , payload:error});
            });
    };
};

const getAllSongs = (info) => {
    return (dispatch) => {
        dispatch({type:types.GET_SONGS_PENDING});

        axios.get(types.MUSICS_API_URL)
            .then(function (response) {
            dispatch({type:types.GET_SONGS_SUCCESS, payload:response.data});
        })
            .catch(function (error) {
                dispatch({type:types.GET_SONGS_REJECTED , payload:error});
            });
    };
};

export {addSongs,getAllSongs};
