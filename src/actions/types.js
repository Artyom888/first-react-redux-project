export const GET_ALL_FRIENDS = 'GET_ALL_FRIENDS';
export const ADD_FRIEND = 'ADD_FRIEND';
export const FAVORITE_FRIEND = 'FAVORITE_FRIEND';
export const DELETE_FRIEND = 'DELETE_FRIEND';
export const FRIENDS_API_URL = 'http://magazine.loc/friends';

export const ADD_SONG_SUCCESS = 'ADD_SONG_SUCCESS';
export const ADD_SONG_PENDING = 'ADD_SONG_PENDING';
export const ADD_SONG_REJECTED = 'ADD_SONG_REJECTED';

export const GET_SONGS_SUCCESS = 'GET_SONGS_SUCCESS';
export const GET_SONGS_PENDING = 'GET_SONGS_PENDING';
export const GET_SONGS_REJECTED = 'GET_SONGS_REJECTED';

export const FIND_SONG_SUCCESS = 'FIND_SONG_SUCCESS';
export const FIND_SONG_PENDING = 'FIND_TRACK';
export const FIND_SONG_REJECTED = 'FIND_SONG_REJECTED';
export const MUSICS_API_URL = 'http://magazine.loc/songs';



