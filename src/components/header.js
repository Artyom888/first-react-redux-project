import React, { Component } from 'react';
import { Menu} from 'antd';
import {Link } from 'react-router-dom';


class TopMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current: 'page header',
        };

    }


    handleClick = (e) => {
        this.setState({
            current: e.key,
        });
    };
    render(){
        return(
            <div>
                <Menu
                    onClick={this.handleClick}
                    selectedKeys={[this.state.current]}
                    mode="horizontal"
                >
                    <Menu.Item >
                        <Link to="/">HOME</Link>
                    </Menu.Item>
                    <Menu.Item >
                        <Link to="/songs">MUSIC SECTION</Link>
                    </Menu.Item>
                    <Menu.Item >
                        <Link to="/music">MUSIC LIST</Link>
                    </Menu.Item>
                    <Menu.Item >
                        <Link to="/friends">FRIENDS LIST</Link>
                    </Menu.Item>
                </Menu>

            </div>
        )

    }

}
export default TopMenu;
