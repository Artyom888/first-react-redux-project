import React, { Component } from 'react';
import {connect} from 'react-redux';
import TopMenu from '../components/header';
import 'antd/dist/antd.css';
import '../css/friends.css';
import { bindActionCreators } from 'redux';
import * as Actions from '../actions/friends';
import {Icon, Button,Modal } from 'antd';
const confirm = Modal.confirm;

class FriendsList extends Component {
    constructor(props){
        super(props);

        this.addFriendHandler = this.addFriendHandler.bind(this);
        this.deleteFriendHandler = this.deleteFriendHandler.bind(this);
        this.favoriteFriendHandler = this.favoriteFriendHandler.bind(this);
    }

    componentWillMount (){
        this.props.Actions.getAllFriends();
    }

    addFriendHandler(event){
        if(this.inputName.value) {
            this.props.Actions.addFriend(this.inputName.value);
            this.inputName.value = '';
        }
    }

    addFriendKeyPressHandler(event){
        if(this.inputName.value && event.key === 'Enter') {
            this.props.Actions.addFriend(this.inputName.value);
            this.inputName.value = '';
        }
    }

    deleteFriendHandler(id){
        let props = this.props;
        confirm({
            title: 'Are you sure delete this task?',
            content: 'Some descriptions',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                props.Actions.deleteFriend(id);
            },
            onCancel() {
                console.log('Cancel');
            },
        });

    }

    favoriteFriendHandler(id,favorite){
        this.props.Actions.favoriteFriend(id,favorite);
    }

    render(){
        console.log(this.props);
        return <div>
            <TopMenu />
            <div className="container">
                <h3 style={{marginTop:20}}>Your Friends List</h3>
                <div className='frListHeader'>
                    <h3>Add your new friend</h3>
                    <input type='text' ref={(input) => {this.inputName = input }} onKeyPress={(e) => this.addFriendKeyPressHandler(e)} />
                    <Button size={'small'} className="addButton"  onClick={(e) => this.addFriendHandler(e)} >
                        <Icon type="plus-circle-o" style={{ fontSize: 16, color: '#5C75B0' }} spin={false} />
                    </Button>

                </div>

                <div className='frListBody' >
                    {
                        this.props.friendList.friends.map((friend,index) =>
                            <div className='frListRow'  key={index} >
                                <h2>{friend.name}</h2>
                                 <Button size={'small'} onClick={(e) => this.deleteFriendHandler(friend.id)}  className="deleteIcon" >
                                        <Icon type="delete" style={{ fontSize: 16, color: '#5C75B0'}}/>
                                 </Button>

                                <Button size={'small'} onClick={(e) => this.favoriteFriendHandler(friend.id,!Boolean(friend.favorite))}  className="starIcon">
                                    <Icon type={Boolean(friend.favorite) ? 'star':'star-o'}  style={{ fontSize: 16, color: '#5C75B0'}}/>
                                </Button>
                                <p>Your Friend</p>
                            </div>
                        )
                    }
                </div>
            </div>
        </div>

    }
}

const mapStateToProps = (state) => ({
    friendList:state.friends
});

const mapDispatchToProps = (dispatch) => ({
    Actions:bindActionCreators(Actions, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FriendsList);
