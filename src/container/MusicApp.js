import React, { Component } from 'react';
import {connect} from 'react-redux';
import '../css/songs.css';
import TopMenu from '../components/header';
import { bindActionCreators } from 'redux';
import * as Actions from '../actions/songs';
import { Icon, Button, message, Progress , Spin} from 'antd';




class MusicApp extends Component {
    constructor(props) {
        super(props);
        this.fileUploadHandler = this.fileUploadHandler.bind(this);
    }

    componentWillMount (){
        this.props.Actions.getAllSongs();
    }

    fileUploadHandler = (file) => {
        const song = this.fileInput.files[0];
        if(song){
            if(song.type !== 'audio/mp3'){
                message.error('You can only upload mp3 file!');
            }else if(song.size / 1000000 > 1024){
                message.error(`Audio must smaller than 10MB! , you upload ${(song.size / 1000000 ).toFixed(1)}MB`);
            }else{
                this.props.Actions.addSongs(this.fileInput.files[0]);
                this.fileInput.value = '';
            }
        }
    };

    render(){
        const {songsList} = this.props;
        return <div>
            <Spin size={'large'} spinning={songsList.spinning} />
            <TopMenu />
            <div>
                {
                    songsList.error ?  message.error('You can only upload mp3 file!')
                    : null
                }

                <Button onClick={(e) => this.fileInput.click()}>
                     Select File
                    <input style={{display:'none'}} type="file" ref={(input) => { this.fileInput = input; }}/>
                </Button>

                <Button onClick={(e) => this.fileUploadHandler(e)}>
                    <Icon type="upload" /> Upload File
                </Button>
                {
                    songsList.progress.show ?
                        <Progress percent={songsList.progress.percent} strokeWidth ={10} showInfo={true} format={percent => `${percent} %`}/>
                        : null
                }
            </div>

            <div>
                <ul>
                    {
                        this.props.songsList.songs && this.props.songsList.songs.map((field , index) =>
                            <li key={index}>{field.name}</li>

                        )

                    }
                </ul>
            </div>
        </div>
    }
}


const mapStateToProps = (state) => ({
    songsList:state.songs,
});

const mapDispatchToProps = (dispatch) => ({
    Actions:bindActionCreators(Actions, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MusicApp);
