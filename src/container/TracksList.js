import React, { Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../actions/tracks';
import TopMenu from '../components/header';



class TracksList extends Component {
    constructor(props){
        super(props);
        this.addTrackHandler = this.addTrackHandler.bind(this);
        this.findTrackHandler = this.findTrackHandler.bind(this);
    }

    addTrackHandler(){
        this.props.Actions.addTracks(this.trackInput.value);
        this.trackInput.value = '';
    }

    findTrackHandler(){
        this.props.Actions.findTracks(this.searchInput.value);
    }

    render() {
        return (
            <div>
                <TopMenu />
                <div className="container">
                    <div>
                        <input type="text" ref={(input) => {this.trackInput = input }}/>
                        <button onClick={this.addTrackHandler}>Add Track </button>
                    </div>

                    <div>
                        <input type="text" ref={(input) => {this.searchInput = input }}/>
                        <button onClick={this.findTrackHandler}>Find Track </button>
                    </div>
                    <div>
                        <button onClick={this.props.Actions.getTracks}>Get Track </button>
                    </div>
                    <ul>
                        {
                            this.props.tracks.map((track,index) =>
                                <li key={index}>{track.name}</li>
                            )
                        }
                    </ul>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    tracks:state.tracks.filter( track => track.name.includes(state.filterTracks))
});

const mapDispatchToProps = (dispatch) => ({
    Actions:bindActionCreators(Actions, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TracksList);