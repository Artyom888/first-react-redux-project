import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import App from './App';
import FriendsList from './container/FriendsList';
import MusicApp from './container/MusicApp';
import TracksList from './container/TracksList';
import { createStore , applyMiddleware } from 'redux';
import { Provider} from 'react-redux';
import reducer from './reducers';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import logger from 'redux-logger'


const store = createStore(reducer,composeWithDevTools(applyMiddleware(logger, thunk)));

ReactDOM.render(
		<Provider store={store}>
            <BrowserRouter >
                <div>
                    <Route exact path={'/'} component={ App }/>
                    <Route exact path={'/songs'} component={ MusicApp }/>
                    <Route exact path={'/music'} component={ TracksList }/>
                    <Route exact path={'/friends'} component={ FriendsList }/>
                </div>
            </BrowserRouter>
		</Provider>,
	 document.getElementById('root'));



