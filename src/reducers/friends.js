import * as types from '../actions/types';


const initialState = {
    friends: [],
};


export default function friends (state = initialState , action) {
    switch(action.type){
        case types.GET_ALL_FRIENDS:
            return {
                friends: action.payload
            };
        case types.ADD_FRIEND:
            return {
                friends: [ action.payload,...state.friends]
            };
        case types.DELETE_FRIEND:
            return {
                friends: [
                    ...state.friends.filter(field => field.id !== action.id)
                ],
            };
        case types.FAVORITE_FRIEND:
            Object.assign({}, state, {
                friends:  state.friends.map((elem) => {
                    if(elem.id === action.field.id) {
                        elem.favorite= Boolean(action.field.favorite);
                    }
                })
            });
            return {...state };
        default :
            return state;
    }
}
