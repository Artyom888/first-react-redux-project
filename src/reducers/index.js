import { combineReducers } from 'redux'
import tracks from './tracks';
import playlists from './playlists';
import filterTracks from './filterTracks';
import friends from './friends';
import songs from './songs';


export default combineReducers({
	tracks,
	playlists,
	filterTracks,
    friends,
    songs
 });