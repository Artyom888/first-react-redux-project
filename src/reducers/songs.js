import * as types from '../actions/types';

const initialState = {
    songs:[],
    progress:{percent:0,show:false},
    spinning:false,
    error: null
};

export function songs(state = initialState, action) {
    switch(action.type) {
        case types.ADD_SONG_PENDING:
            return {
                ...state,
                progress:{percent:action.payload,show:true},
            };
        case types.ADD_SONG_SUCCESS:
            return {
                ...state,
                songs:[...state.songs , action.payload],
                progress:{percent:100,show:false},
            };
        case types.ADD_SONG_REJECTED:
            return {
                ...state,
                error: action.payload
            };
        // GET SONGS IN DB
        case  types.GET_SONGS_PENDING:
            return {
                ...state,
                spinning:true
            };
        case types.GET_SONGS_SUCCESS:
            return {
                ...state,
                spinning:false,
                songs:action.payload,
            };
        case types.GET_SONGS_REJECTED:
            return {
                ...state,
                spinning: false,
                error: action.payload
            };

        default:
            return state;
    }
}


export default songs;